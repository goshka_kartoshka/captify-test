const redis = require('redis');

const client = redis.createClient(6379, 'redis-server');
client.on('error', err => {
  console.log(`Redis error: ${err}`);
});

module.exports = client;
