const express = require('express');
const cors = require('cors');
const helmet = require('helmet');
const expressSanitizer = require('express-sanitizer');
const routes = require('./routes');

const app = express();
const port = process.env.PORT || 5000;

app.use(helmet());
app.use(expressSanitizer());
app.use(cors());
app.use(express.json());
app.use('/', routes);
app.listen(port, () => console.log(`Listening on port ${port}`));

module.exports = app;
