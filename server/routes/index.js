const express = require('express');
const client = require('../storage');

const router = express.Router();
const key = 'rows';
const maxLength = 10;

router.route('/rows').get((req, res) => {
  client.lrange(key, 0, -1, (error, data) => {
    if (error) res.sendStatus(400);

    const rows = data.map((row, index) => {
      return { index: index, text: row };
    });
    res.status(200).json(rows);
  });
});

router.route('/rows').post((req, res) => {
  const newRow = req.sanitize(req.body.inputText);
  client.lpush([key, newRow], error => {
    if (error) res.sendStatus(400);
    res.sendStatus(200);
  });
  client.ltrim(key, 0, maxLength - 1);
});

module.exports = router;
