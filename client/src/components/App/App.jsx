import React from 'react';
import axios from 'axios';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Typography from '@material-ui/core/Typography';
import classes from './App.module.scss';

class App extends React.Component {
  state = {
    inputText: '',
    rows: []
  }

  componentDidMount = async () => {
    this.getAllRows();
  }

  getAllRows = async () => {
    try {
      const response = await axios.get('http://127.0.0.1:5000/rows');
      if (response.data) {
        this.setState({ rows: response.data });
      }
    } catch (error) {
      alert(error);
    }
  }

  handleInputText = event => {
    this.setState({ inputText: event.target.value });
  }

  addRow = async () => {
    const { inputText } = this.state;

    if (inputText.split(' ').join('') !== '') {
      this.setState({ inputText: '' });
      try {
        await axios.post('http://127.0.0.1:5000/rows', { inputText });
        this.getAllRows();
      } catch (error) {
        alert(error);
      }
    }
  }

  render() {
    const { rows, inputText } = this.state;

    return (
      <div className={classes.app}>
        <Container maxWidth="sm">
          <Paper className={classes.inputWrap}>
            <TextField
              margin="normal"
              variant="outlined"
              className={classes.textInput}
              value={inputText}
              onChange={this.handleInputText}
            />
            <Fab
              color="primary"
              aria-label="Add"
              disabled={inputText === ''}
              onClick={this.addRow}
            >
              <AddIcon />
            </Fab>
          </Paper>

          <Paper className={classes.tableWrap}>
            {rows.length > 0 ? (
              <Table>
                <TableBody>
                  {rows.map(row => (
                    <TableRow key={row.index}>
                      <TableCell>
                        {row.text}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            ) : (
                <Typography variant="h2" gutterBottom>
                  Table is empty
                </Typography>
              )}
          </Paper>
        </Container>
      </div>
    );
  }
};

export default App;
