# Project Title

Dev version of test app.

## Getting Started

For starting this app on your local machine you need Docker and docker-compose.
Run
```
docker-compose up --buld
```
for first time or
```
docker-compose up
```
if you've already built the app

## Running the tests

In theory to run tests you need to open docker container client or server and run test command.
```
docker exec -it container_name sh
```
Find container name using
```
docker ps
```
command.

### And coding style tests

In this project linter checking starts by pre-commit hook. You don't need to run any specific command.
